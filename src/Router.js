import React, { Component } from 'react';
import { BrowserRouter, Routes, Route, useParams } from 'react-router-dom';


import Header from './assets/components/MainSite/Header';
import Home from './assets/components/MainSite/Home';
import Footer from './assets/components/MainSite/Footer';
import ShoppingCart from './assets/components/ShoppingCart';
import Clients from './assets/components/Clients';
import QuoteHistory from './assets/components/QuoteHistory';
import BuildMaterial from './assets/components/BuildMaterial';

class Router extends Component {

    render() {

        return (
            // configurar rutas y páginas version 6 en adelante de react router dom
            <BrowserRouter>
                {/* llamos a la barra de menú NAV <Header/>*/}
                <Header />
                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route exact path="/home" element={<Home />} />
                    <Route exact path="/buildmaterial" element={<BuildMaterial />} />
                    <Route exact path="/shoppingcart" element={<ShoppingCart />} />
                    <Route exact path="/quotehistory" element={<QuoteHistory />} />
                    <Route exact path="/clients" element={<Clients />} />
                </Routes>
                <Footer />
            </BrowserRouter>
        );
    }
}

export default Router;