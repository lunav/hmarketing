import logo from './logo.svg';
import './App.css';
//cargamos el componete que se encargará del Routing
import Router from './Router';


function App() {
  return (
    <div className="App">
    <Router />
  </div>
  );
}

export default App;
