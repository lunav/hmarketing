import React, { useState, useEffect } from 'react';
//importando URL
import Global from '../../Global';
import axios from 'axios';
// Importando libreria para el uso de tablas (componente)
import { Button, Modal } from 'react-bootstrap';
//Complemento que contiene el div de material para pintarlo.
import Material from './Material';

function BuildMaterial() {

  //importando URL de Global
  let url = Global.url;
  const [status, setStatus] = useState(null);
  const [materials, setMaterials] = useState({});
  const [materialsToApi, setMaterialsToApi] = useState({});
  const [materialsToSearch, setMaterialsToSearch] = useState({});
  // Paramostrar el modal
  const [show, setShow] = useState(false);
  // Variables para obtener la referencia de cada input
  var nameRef = React.createRef();
  var descriptionRef = React.createRef();
  var stockRef = React.createRef();
  var priceRef = React.createRef();
  var idRef = React.createRef();
  var searchRef = React.createRef();


  useEffect(() => {
    // Corro el script antes de que se cargue el component, 
    // pero sigue siendo más rápida la llamada de renturn y se valida que el state tenga 
    // valor en el html
    if (localStorage.getItem('materials')) {
      // Si tiene valor la cookie ya no consume el api
      setMaterials(JSON.parse(localStorage.getItem('materials')));
    } else {
      apiRead()
    }

  }, []);

  function apiRead() {

    axios.get(url + 'material/all')
      .then(res => {
        // le doy valor al state, que contiene el status y el obj array.
        setMaterials(res.data)
        localStorage.setItem('materials', JSON.stringify(res.data));
      })
  }


  function updateApi(material, indice) {
    setShow(true);
    setMaterialsToApi(material);
    setStatus('update');
  }

  // Para mostrar o no el modal
  function handleShow(breakpoint) {
    setShow(true);
    setMaterialsToApi({});
  }

  // Para mostrar o no el modal
  function handleShowOff() {
    setShow(false);
    apiRead();
    // setMaterials(cookie.data);
  }

  function saveData(e) {
    e.preventDefault();
    // rellenar state con formulario
    var accionUrl = 'material/add';
    if (status === 'update') {
      accionUrl = 'material/update';
    }

    axios.post(url + accionUrl, materialsToApi)
      .then(res => {
        if (res.data.response) {
          setStatus('success');
          setShow(false);
          apiRead();

        } else {
          setStatus('failed');
          alert('Material Guardado:' + res);
          console.err('Cannot be saved:' + res)
        }

      }).catch(err => {
        alert(err);
        console.err(err);
      });

  }

  // Con esta funcion se va a estar actualizando el state dependiendo del input (todos los inputs)
  function changeState() {

    setMaterialsToApi({
      id: idRef.current.value,
      name: nameRef.current.value,
      description: descriptionRef.current.value,
      stock: stockRef.current.value,
      price: priceRef.current.value
    });
  }

  // The best search create by friends!!!
  function SearchBarMaterials() {
    if(searchRef.current.value!=""){
    var searchtext = searchRef.current.value;
    var b = materials.response;
    let items = b.filter(item => item.name.toString().toLowerCase().includes(searchtext.toLowerCase()));
    setMaterials({ ...materials, ['response']: items });}
    else {
      setMaterials(JSON.parse(localStorage.getItem('materials')))
    }
  }


  return (

    <React.Fragment>
      <div className="container">
        <div className="contentbar mb-2"></div>

        <div className="input-group mb-2">
          <input type="text" className="form-control" placeholder="Ingrese material a buscar" aria-describedby="button-addon2" name='search' ref={searchRef} onChange={SearchBarMaterials}/>
        </div>

        <Modal show={show} onHide={handleShowOff}>
          <Modal.Header closeButton>
            <Modal.Title>Nuevo Material</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <form onSubmit={saveData}>
              <div className="d-none">
                <label htmlFor="id">Nombre:</label>
                <input type="text" className="form-control" id="id" defaultValue={materialsToApi.id} name="id" ref={idRef} />
              </div>
              <div className="mb-1">
                <label htmlFor="name">Nombre:</label>
                <input type="text" className="form-control" id="name" defaultValue={materialsToApi.name} name="name" ref={nameRef} onChange={changeState} required />
              </div>
              <div className="mb-1">
                <label htmlFor="phone">Descripción:</label>
                <input type="text" className="form-control" id="description" defaultValue={materialsToApi ? (materialsToApi.description) : ''} placeholder="" name="description" ref={descriptionRef} onChange={changeState} />
              </div>
              <div className="mb-1">
                <label htmlFor="price">Precio:</label>
                <input type="number" className="form-control" id="price" placeholder="$" defaultValue={materialsToApi ? (materialsToApi.price) : ''} name="price" ref={priceRef} onChange={changeState} required />
              </div>
              <div className="mb-1">
                <label htmlFor="stock">Stock:</label>
                <input type="number" className="form-control" min="0" id="stock" placeholder="Cantidad en Inventario" defaultValue={materialsToApi.stock ? (materialsToApi.stock) : '0'} name="stock" ref={stockRef} onChange={changeState} />
              </div>
              <button type="submit" className="btn btn-primary ">Guardar</button>
            </form>
          </Modal.Body>
        </Modal>

        {/* boton para agregar un nuevo material */}
        <button type="button" className="btn btn-primary mt-2" onClick={() => handleShow()}>Dar de Alta Material</button>
        <div className="grey-bg container-fluid">
          <section id="minimal-statistics">
            <div className="row">
              <div className="col-12 mt-3 mb-1">
                <h4 className="text-uppercase">Materiales</h4>
                <p>Registrados</p>
              </div>
            </div>
            <div className="row">
              {/* validamos si tiene valor el estado materials */}
              { }
              {materials.status === 'success' &&
                <React.Fragment>
                  {materials.response.map((material, index) => {
                    return (
                      <Material
                        key={index}
                        material={material}
                        indice={index}
                        update={updateApi}
                      />
                    )
                  })}
                </React.Fragment>
              }
            </div>
          </section>
        </div>
      </div>

    </React.Fragment>
  )
}

export default BuildMaterial;