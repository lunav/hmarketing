import React, { useState, useEffect } from 'react';
// Importación de la URL para usarla de forma corta.
import Global from '../../../src/Global';
//Libreria para usar fetch igual puedes usar fecth de javascript
import axios from 'axios';
//Libreria para mostrar popups amigables
import Swal from 'sweetalert2'
//Libreria para agregar asincronamente informacion a un select
import AsyncSelect from 'react-select/async';
//Libreria para iconos de react
import { MdRemoveCircleOutline, MdAddCircleOutline, MdDelete } from 'react-icons/md';
//Librerias para hacer un dropdownlist donde puedas buscar un elemento
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';

function ShoppingCart() {
  const [items, setItems] = useState([]);
  const [selectedValue, setSelectedValue] = useState(null);
  const [applyIva, setApplyIva] = useState(true);

  const [priceValues, setPriceValues] = useState({
    laborCost: 0,
    subTotal: 0,
    iva: 0,
    total: 0
  });

  const [open, setOpen] = useState(false);
  const [options, setOptions] = useState([]);
  const loading = open && options.length === 0;

  var descriptionRef = React.createRef();

  let url = Global.url;

  const fetchData = () => {

    if (localStorage.getItem('materials')) {
      const cache = JSON.parse(localStorage.getItem('materials'));
      let data = processToSelect(cache);
      setOptions(data);
    } else {
      let data = axios.get(url + 'material/all')
        .then(res => {
          return processToSelect(res.data)
        })
      setOptions(data);
    }
  }

  function processToSelect(obj) {
    const data = obj.response.map(item => ({
      ...item,
      quantity: 1,
      total: item.price,
    }));
    return data;
  }

  function handleCheckBoxChange() {
    let l_total = 0;
    if (!applyIva) {
      l_total = priceValues.total + priceValues.iva;
    }
    else {
      l_total = priceValues.total - priceValues.iva;
    }

    setPriceValues({
      ...priceValues,
      'total': l_total
    });

    setApplyIva(!applyIva);
  };

  const handleChange = (event, value) => setSelectedValue(value);

  function handleItemDeleted(i) {
    var oItems = [...items];

    oItems.splice(i, 1);

    calculatePrices(oItems);
    setItems(oItems);
  }

  function increment(index) {
    var oItems = [...items];
    oItems[index].quantity++;
    oItems[index].total = oItems[index].quantity * oItems[index].price;

    calculatePrices(oItems);
    setItems(oItems);
  }

  function decrement(index) {
    var oItems = [...items];
    if (oItems[index].quantity > 1) {
      oItems[index].quantity--;
      oItems[index].total = oItems[index].quantity * oItems[index].price;

      calculatePrices(oItems);
      setItems(oItems);
    }
  }

  function handleClickAddMaterial() {

    var oItems = [...items];

    if (oItems.some(item => selectedValue.name === item.name)) {
      Swal.fire({
        title: 'Error!',
        text: 'Este material ya existe en la lista de cotizacion',
        icon: 'error',
        confirmButtonText: 'OK'
      })
      return;
    }
    if (selectedValue !== null) {
      oItems.push(selectedValue);
      calculatePrices(oItems);
      setItems(oItems);
    }
  }
  function handleClickAddGetPDF() {
    Swal.fire({
      title: '!! Generando PDF !!',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading()
      },
    })
    axios({
      url: url + 'pdfdownload',
      method: 'POST',
      responseType: 'arraybuffer', // important
      data: {
        total: formatPrice(priceValues.total),
        subTotal: formatPrice(priceValues.subTotal),
        iva: applyIva ? formatPrice(priceValues.iva) : 0.00,
        description: descriptionRef.current.value,
      },
    }).catch(function (error) {
      // handle error
      console.log(error);
      Swal.close()
    })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = url;
        let today = new Date();
        let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
        let time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
        let filename = 'Cotizacion_' + date + '_' + time + '.pdf'
        link.setAttribute('download', filename);
        document.body.appendChild(link);
        link.click();
        Swal.close()
      });
  }

  function calculatePrices(items) {
    let l_subtotal = 0;
    let l_iva = 0;
    let l_total = 0;

    var i;
    for (i = 0; i < items.length; i++) {
      l_subtotal += parseInt(items[i].total);
    }

    l_subtotal += parseInt(priceValues.laborCost);

    l_iva = (l_subtotal * 16) / 100;

    if (applyIva)
      l_total = l_subtotal + l_iva;
    else
      l_total = l_subtotal;

    setPriceValues({
      ...priceValues,
      'total': l_total,
      'subTotal': l_subtotal,
      'iva': l_iva
    });
  }

  function formatPrice(price) {
    return parseFloat(price).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  return (
    <React.Fragment>
      <div className="container">
        <div className="contentbar">
          <div className="row">
            <div className="col-md-12 col-lg-12 col-xl-12">
              <div className="card m-b-30">
                <div className="card-header">
                  <h5 className="card-title">cotizacion</h5>
                </div>
                <div className="card-body">
                  <div className="row justify-content-center">
                    <div className="col-lg-10 col-xl-8">
                      <div className="cart-container">
                        <div className="cart-head">
                          <div className="form-group">
                            <div className="input-group search-material">
                              <AsyncSelect
                                className="form-select d-none"
                                cacheOptions
                                defaultOptions
                                value={selectedValue}
                                getOptionLabel={e => e.name}
                                getOptionValue={e => e}
                                loadOptions={fetchData}
                                onChange={handleChange}
                              />
                              <Autocomplete
                                id="search-material"
                                sx={{ width: 300 }}
                                open={open}
                                onOpen={() => {
                                  setOpen(true);
                                }}
                                onClose={() => {
                                  setOpen(false);
                                }}
                                isOptionEqualToValue={(option, value) => option.name === value.name}
                                getOptionLabel={(option) => option.name}
                                onChange={handleChange}
                                options={options}
                                loading={loading}
                                renderInput={(params) => (
                                  <TextField
                                    {...params}
                                    label="Materiales"
                                    InputProps={{
                                      ...params.InputProps,
                                      endAdornment: (
                                        <React.Fragment>
                                          {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                          {params.InputProps.endAdornment}
                                        </React.Fragment>
                                      ),
                                    }}
                                  />
                                )}
                              />
                              <div className="input-group-append">
                                <button className="input-group-text" id="btn-add-material" onClick={() => handleClickAddMaterial()}>Agregar Material</button>
                              </div>
                            </div>
                          </div>
                          <div className="table-responsive">
                            <table className="table table-borderless">
                              <thead>
                                <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Acciones</th>
                                  <th scope="col">Material</th>
                                  <th scope="col">Cantidad</th>
                                  <th scope="col">Precio</th>
                                  <th scope="col" className="text-right">Total</th>
                                </tr>
                              </thead>
                              <tbody>

                                {items.map((item, index) => {
                                  return (
                                    <tr key={"item-" + index}>
                                      <th scope="row">{index + 1}</th>
                                      <td>
                                        <button className="btn-icon" type="button" onClick={() => handleItemDeleted(index)}><MdDelete size={30} color="#FF7588" /></button>
                                      </td>
                                      <td>{item.name}</td>
                                      <td>
                                        <button className="btn-icon" type="button" onClick={() => decrement(index)}>
                                          <MdRemoveCircleOutline size={20} color="#FF7588" />
                                        </button>
                                        <input className="input-quantity text-center" type="number" readOnly value={item.quantity} />
                                        <button className="btn-icon" type="button" onClick={() => increment(index)}>
                                          <MdAddCircleOutline size={20} color="#16D39A" />
                                        </button>
                                      </td>
                                      <td>${formatPrice(item.price)}</td>
                                      <td className="text-right">${formatPrice(item.total)}</td>
                                    </tr>
                                  )
                                })}

                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div className="cart-body">
                          <div className="row">
                            <div className="col-md-12 order-2 order-lg-1 col-lg-5 col-xl-6">
                              <div className="order-note">
                                <form>

                                  <div className="form-group">
                                    <label htmlFor="description">Notas:</label>
                                    <textarea className="form-control" name="description" id="description" ref={descriptionRef} rows="3" placeholder="Escriba aqui"></textarea>
                                  </div>
                                </form>
                              </div>
                            </div>
                            <div className="col-md-12 order-1 order-lg-2 col-lg-7 col-xl-6">
                              <div className="order-total table-responsive ">
                                <table className="table table-borderless text-right">
                                  <tbody>
                                    <tr>
                                      <td>Sub Total <small>(Materiales)</small> :</td>
                                      <td>${formatPrice(priceValues.subTotal)}</td>
                                    </tr>
                                    <tr>
                                      <td>Mano de obra :</td>
                                      <td>${formatPrice(priceValues.laborCost)}</td>
                                    </tr>
                                    <tr>
                                      <td>
                                        <div className="form-group">
                                          <input className="form-check-input" id="checkboxIva" type="checkbox" checked={applyIva} onChange={handleCheckBoxChange} />
                                          <label className="form-check-label" htmlFor="checkboxIva">IVA(16%):</label>
                                        </div>
                                      </td>
                                      <td>{applyIva ? '$' + formatPrice(priceValues.iva) : <del>${formatPrice(priceValues.iva)}</del>}</td>
                                    </tr>
                                    <tr>
                                      <td className="f-w-7 font-18"><h4>TOTAL :</h4></td>
                                      <td className="f-w-7 font-18"><h4>${formatPrice(priceValues.total)}</h4></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="cart-footer text-right">
                          <button className="btn btn-success text-center my-1" onClick={() => handleClickAddGetPDF()}>Generar PDF</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <hr />
    </React.Fragment>
  )
}

export default ShoppingCart;