// se va desplegar un div con la información del material.
import React, { useState, useEffect } from 'react';
// Importación de la URL para usarla de forma corta.
import Global from '../../../src/Global';
//Libreria para usar fetch igual puedes usar fecth de javascript
import axios from 'axios';

//habilitamos la funcion para recibir props/funciones
function Material(props) {
    var url = Global.url;

    const material = props.material;

    function DataToFatherComponent() {
        props.update(props.material, props.index)
    }
    const [client, setClient] = useState({});
    const [status, setStatus] = useState(null);

    // Variables para obtener la referencia de cada input
    var nameRef = React.createRef();
    var phoneRef = React.createRef();
    var emailRef = React.createRef();

    function saveData(e) {
        e.preventDefault();
        // rellenar state con formulario
        axios.post(url + 'client/add', client)
            .then(res => {
                if (res.data.response) {

                    setStatus('success');
                    props.refreshTable(true);
                    alert('Cliente Guardado')

                } else {
                    setStatus('failed');
                    console.log('Cannot be saved:' + res)
                }

            }).catch(err => {
                console.log(err);
            });

    }

    // Con esta funcion se va a estar actualizando el state dependiendo del input (todos los inputs)
    function changeState() {
        setClient({
            name: nameRef.current.value,
            phone: phoneRef.current.value,
            email: emailRef.current.value
        })
    }
    return (
        <React.Fragment>
            <div key={1} className="col-xl-3 col-sm-6 col-12" onClick={DataToFatherComponent}>
                <div className="card">
                    <div className="card-content">
                        <div className="card-body">
                            <div className="media d-flex">
                                <div className="align-self-center">
                                    <i className="icon-pencil primary font-large-2 float-left"></i>
                                </div>
                                <div className="media-body text-right">
                                    <h3>{props.material.name}</h3>
                                    <h5>Precio {props.material.price}</h5>
                                    <span>Stock {props.material.stock}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Material;