import React, { useState, useEffect } from 'react';
// Importación de la URL para usarla de forma corta.
import Global from '../../../src/Global';
//Libreria para usar fetch igual puedes usar fecth de javascript
import axios from 'axios';

//habilitamos la funcion para recibir props/funciones
function RecordClient(props) {
  var url = Global.url;
  const [client, setClient] = useState({});
  const [status, setStatus] = useState(null);

  // Variables para obtener la referencia de cada input
  var nameRef = React.createRef();
  var phoneRef = React.createRef();
  var emailRef = React.createRef();


  function saveData(e) {
    e.preventDefault();
    // rellenar state con formulario
    axios.post(url + 'client/add', client)
      .then(res => {
        if (res.data.response) {

          setStatus('success');
          props.refreshTable(true);
          alert('Cliente Guardado')

        } else {
          setStatus('failed');
          console.log('Cannot be saved:' + res)
        }

      }).catch(err => {
        console.log(err);
      });

  }

  // Con esta funcion se va a estar actualizando el state dependiendo del input (todos los inputs)
  function changeState() {
    setClient({
      name: nameRef.current.value,
      phone: phoneRef.current.value,
      email: emailRef.current.value
    })
  }
  return (
    <React.Fragment>
      <h2>Stacked form</h2>
      <form onSubmit={saveData}>
        <div className="mb-3 mt-3">
          <label htmlFor="name">Nombre:</label>
          <input type="text" className="form-control" id="name" placeholder="" name="name" ref={nameRef} onChange={changeState} />
        </div>
        <div className="mb-3">
          <label htmlFor="phone">Teléfono:</label>
          <input type="number" className="form-control" id="phone" placeholder="" name="phone" ref={phoneRef} onChange={changeState} />
        </div>
        <div className="mb-3">
          <label htmlFor="email">Correo:</label>
          <input type="email" className="form-control" id="email" placeholder="Enter password" name="email" ref={emailRef} onChange={changeState} />
        </div>
        <button type="submit" className="btn btn-primary">Guardar</button>
      </form>
    </React.Fragment>
  )
}

export default RecordClient;