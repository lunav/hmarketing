import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { ProgressBar } from 'react-bootstrap';
import axios from 'axios';

function Home() {
    const naviate = useNavigate();


    function sendBuildMaterial() {
        console.log('click')
        return naviate('/buildmaterial')
    }
        function sendShoppingCart() {
        console.log('click')
        return naviate('/shoppingcart')
    }
        function sendClients() {
        console.log('click')
        return naviate('/clients')
    }

    return (
        <React.Fragment>
            <div className="container">
                <div className="contentbar">
                    <div className="row">
                        <div className="col-md-12 col-lg-12 col-xl-12">
                            <div className="card m-b-30">
                                <div className="card-header text-center">
                                    <h5 className="card-title">Bienvenido</h5>
                                </div>
                                <div className="card-body">
                                    <div className="row justify-content-center">
                                        <div className="col-xl-3 col-sm-6" onClick={sendShoppingCart}>
                                            <div className="card l-bg-cherry">
                                                <div className="card-statistic-3 p-1">
                                                    <div className="card-icon card-icon-large"><i className="fas fa-shopping-cart"></i></div>
                                                    <div className="mb-4">
                                                        <h5 className="card-title mb-0">Nueva Cotización</h5>
                                                    </div>
                                                    <div className="row align-items-center mb-2 d-flex">
                                                        <div className="col-8">
                                                            <h2 className="d-flex align-items-center mb-0">
                                                                3,243
                                                            </h2>
                                                        </div>
                                                        <div className="col-4 text-right">
                                                            <span>12.5% <i className="fa fa-arrow-up"></i></span>
                                                        </div>
                                                    </div>
                                                    <ProgressBar animated now={12.5} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-sm-6">
                                            <div className="card l-bg-blue-dark">
                                                <div className="card-statistic-3 p-1" onClick={sendClients}>
                                                    <div className="card-icon card-icon-large"><i className="fas fa-users"></i></div>
                                                    <div className="mb-4">
                                                        <h5 className="card-title mb-0">Clientes</h5>
                                                    </div>
                                                    <div className="row align-items-center mb-2 d-flex">
                                                        <div className="col-8">
                                                            <h2 className="d-flex align-items-center mb-0">
                                                                15.07k
                                                            </h2>
                                                        </div>
                                                        <div className="col-4 text-right">
                                                            <span>9.23% <i className="fa fa-arrow-up"></i></span>
                                                        </div>
                                                    </div>
                                                    <ProgressBar animated now={9.23} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-sm-6">
                                            <div className="card l-bg-green-dark">
                                                <div className="card-statistic-3 p-1" onClick={sendBuildMaterial}>
                                                    <div className="card-icon card-icon-large"><i className="fas fa-ticket-alt"></i></div>
                                                    <div className="mb-4">
                                                        <h5 className="card-title mb-0">Materiales</h5>
                                                    </div>
                                                    <div className="row align-items-center mb-2 d-flex">
                                                        <div className="col-8">
                                                            <h2 className="d-flex align-items-center mb-0">
                                                                578
                                                            </h2>
                                                        </div>
                                                        <div className="col-4 text-right">
                                                            <span>10% <i className="fa fa-arrow-up"></i></span>
                                                        </div>
                                                    </div>
                                                    <ProgressBar animated now={10} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-sm-6">
                                            <div className="card l-bg-orange-dark">
                                                <div className="card-statistic-3 p-1">
                                                    <div className="card-icon card-icon-large"><i className="fas fa-dollar-sign"></i></div>
                                                    <div className="mb-4">
                                                        <h5 className="card-title mb-0">Ganancias</h5>
                                                    </div>
                                                    <div className="row align-items-center mb-2 d-flex">
                                                        <div className="col-8">
                                                            <h2 className="d-flex align-items-center mb-0">
                                                                $11.61k
                                                            </h2>
                                                        </div>
                                                        <div className="col-4 text-right">
                                                            <span>2.5% <i className="fa fa-arrow-up"></i></span>
                                                        </div>
                                                    </div>
                                                    <ProgressBar animated now={2.5} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Home;