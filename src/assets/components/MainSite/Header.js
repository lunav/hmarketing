import React, { useState, useEffect } from 'react';
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap'
import logo from '../../images/logo.svg';
//navlink para que no recargue el navegador cuando se cambia entre paginas.
import { NavLink } from 'react-router-dom';
// Libreria para consumir apis
import axios from 'axios';
//importando URL
import Global from '../../../Global';

function Header() {
    const [isNavbarOpen, setNavbarOpen] = useState(false);

      //importando URL de Global
  let url = Global.url;

  useEffect(() => {
      // Cargo el obj en variable local storage
      if (localStorage.getItem('materials')) {
          console.log('Load correctly Data...')
      } else {
          apiRead()
      }
  
  }, []);
  
  
  function apiRead() {
  
      axios.get(url + 'material/all')
          .then(res => {
              // le doy valor al state, que contiene el status y el obj array.
              localStorage.setItem('materials', JSON.stringify(res.data));
          })
  }
    return (
        <React.Fragment>
            <div className="p-2 bg-primary text-white text-center">
                <h1>Hellen Marketing</h1>
                <p>Software de Cotización</p>
            </div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Toggle className="nav-center" aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="nav-center">
                            <NavLink className="nav-link" to="/home">Inicio</NavLink>
                            <NavLink className="nav-link" to="/buildmaterial">Materiales de Construcción</NavLink>
                            <NavLink className="nav-link" to="/shoppingcart">Cotizar</NavLink>
                            <NavLink className="nav-link" to="/quotehistory">Historial</NavLink>
                            <NavLink className="nav-link" to="/clients">Clientes</NavLink>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </React.Fragment>
    );
}

export default Header;