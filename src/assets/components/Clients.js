import React, { useState, useEffect } from 'react';
// Importando libreria para el uso de tablas (componente)
import { Table } from 'react-bootstrap';
// Importación de la URL para usarla de forma corta.
import Global from '../../../src/Global';
//Libreria para usar fetch igual puedes usar fecth de javascript
import axios from 'axios';
// Importando componente de formulario para creación de cliente
import RecordClient from './RecordClient';

function Clients() {
  // Creo los estados para darle uso posterior
  const [clients, setClients] = useState({});
  // Importando URL de Global
  let url = Global.url;

  useEffect(() => {
    // Corro el script antes de que se cargue el component, 
    // pero sigue siendo más rápida la llamada de renturn y se valida que el state tenga 
    // valor en el html
    apiRead();
  }, []);

  function refreshTable(flag) {
    if (true) {
      apiRead();
    }
  }

  function apiRead() {
    axios.get(url + 'client/all')
      .then(res => {
        // le doy valor al state Clients, que contiene el status y el obj array.
        setClients(res.data)
      })
  }

  return (
    <React.Fragment>
      <div className="container">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Correo</th>
            </tr>
          </thead>
          <tbody>
            {/* verfico si la respuesta del api es success para mostrar el valor del state en forma de map */}
            {clients.status === 'success' &&
              <React.Fragment>
                {clients.response.map((client, index) => {
                  return (
                    <tr key={client.id}>
                      <td>{index + 1}</td>
                      <td>{client.name}</td>
                      <td><a href={'tel:' + client.phone}>{client.phone}</a></td>
                      <td><a href={'mailto:' + client.email}>{client.email}</a></td>
                    </tr>
                  )
                })}
              </React.Fragment>
            }
          </tbody>
        </Table>
        {/* Mando a llamar al formulario */}
        <RecordClient
          refreshTable={refreshTable}
        />
      </div>
    </React.Fragment>
  )
}

export default Clients;